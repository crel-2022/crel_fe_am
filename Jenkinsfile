pipeline {
  agent {
    docker {
      image 'node:14.5.0'
      args '-u root:root'
    }
  }

  environment {
    dockerHome = tool 'myDocker'
    PATH = "${dockerHome}/bin:${env.PATH}"
  }

  stages {
    stage('Build') {
      steps {
        sh 'apt-get update && apt-get install -y ssh-client bash'
        script {
          withCredentials([
            sshUserPrivateKey(credentialsId: 'SSH_PRIVATE_KEY', keyFileVariable: 'SSH_PRIVATE_KEY', passphraseVariable: '', usernameVariable: 'SSH_USER'),
            string(credentialsId: 'DOCKER_TOKEN', variable: 'DOCKER_TOKEN'),
            string(credentialsId: 'SSH_USER', variable: 'SSH_USER'),
            string(credentialsId: 'SSH_SERVER_IP', variable: 'SSH_SERVER_IP'),
            string(credentialsId: 'DOCKER_USER', variable: 'DOCKER_USER')
          ]) {
            sshagent(['SSH_PRIVATE_KEY']) {
              sh 'mkdir -p ~/.ssh'
              sh 'ssh-keyscan -H $SSH_SERVER_IP >> ~/.ssh/known_hosts'
              sh 'chmod 644 ~/.ssh/known_hosts'
              sh 'ssh $SSH_USER@$SSH_SERVER_IP "cd /var/lib/jenkins/workspace/CREL-AM && docker build --no-cache -t $DOCKER_USER/crel_2022:am . && \
                                                docker login -u $DOCKER_USER -p $DOCKER_TOKEN && \
                                                docker push $DOCKER_USER/crel_2022:am"'
            }
          }
        }
      }
    }

    stage('Deploy') {
      steps {
        sh 'apt-get update && apt-get install -y ssh-client bash'
        script {
          withCredentials([
            sshUserPrivateKey(credentialsId: 'SSH_PRIVATE_KEY', keyFileVariable: 'SSH_PRIVATE_KEY', passphraseVariable: '', usernameVariable: 'SSH_USER'),
            string(credentialsId: 'DOCKER_TOKEN', variable: 'DOCKER_TOKEN'),
            string(credentialsId: 'SSH_USER', variable: 'SSH_USER'),
            string(credentialsId: 'SSH_SERVER_IP', variable: 'SSH_SERVER_IP'),
            string(credentialsId: 'DOCKER_USER', variable: 'DOCKER_USER')
          ]) {
            sshagent(['SSH_PRIVATE_KEY']) {
              sh 'mkdir -p ~/.ssh'
              sh 'ssh-keyscan -H $SSH_SERVER_IP >> ~/.ssh/known_hosts'
              sh 'chmod 644 ~/.ssh/known_hosts'
              sh 'ssh $SSH_USER@$SSH_SERVER_IP "docker-compose -f docker-compose.yaml up -d crel-brand && \
                                                while ssh $SSH_USER@$SSH_SERVER_IP "docker ps | grep -q \'docker-builder\'"; do \
                                                echo "Docker builder is still running."; \
                                                sleep 5; \
                                                done; \
                                                echo "Docker builder has finished running. Proceeding with prune."; \
                                                docker builder prune --force && docker image prune --force"'
            }
          }
        }
      }
    }
    stage('Cleanup') {
      steps {
        deleteDir()
      }
    }
  }
}
